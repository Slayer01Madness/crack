#include <stdio.h>
#include "md5.h"
#include <string.h>
#include <stdlib.h>

int main(int argc, char *argv[])
{
    if (argc == 1)
    {
        printf("You must supply two filenames\n");
        exit(1);
    }
    else if (argc == 2)
    {
        printf("You must supply two filenames\n");
        exit(1);
    }
    
    int same_checker = strcmp(argv[1], argv[2]);
    if(same_checker == 0)
    {
        printf("Both files are the same, please use two different files\n");
        exit(1);
    }
    
    FILE *fp1;
    fp1 = fopen(argv[1], "r");
    if (!fp1)
    {
        printf("Can't open %s for reading\n", argv[1]);
        exit(1);
    }
    
    FILE *fp2;
    fp2 = fopen(argv[2], "w");
    if (!fp2)
    {
        printf("Can't open %s for reading\n", argv[2]);
        exit(1);
    }
    
    int len = 0;
    char line[100];
    while(fgets(line, 100, fp1) != NULL)
    {
        len = strlen(line);
        char *hasher = md5(line, len - 1);
        fprintf(fp2, "%s\n", hasher);
    }
    fclose(fp1);
    fclose(fp2);
}